---
id: 68
title: Contact
date: 2014-07-17T18:24:21+00:00
author: jjtroberts
layout: page
guid: http://www.competethemes.com/tracks-two-column-demo/?page_id=68
---
This is an example contact page using the Simple Basic Contact Form plugin. This page is for demonstration only, so please don’t use this form to get in touch with us about Author.

[simple\_contact\_form]
---
id: 61
title: About
date: 2014-07-17T18:20:56+00:00
author: jjtroberts
layout: page
guid: http://www.competethemes.com/tracks-two-column-demo/?page_id=61
---
Author is designed for publishers who want readers. That&#8217;s why Author is fast, responsive, accessibility-ready, and optimally designed for reading.

## Features

Author comes with the following features.

### Custom Avatar

Upload an avatar to display at the top of your sidebar, or automatically display the Gravatar image associated with your admin email account.

### Social media icons

Have some profiles to share? Easily add a linked icon to the header for any of the social sites you&#8217;re on.

### Custom logo

You can add your logo in two clicks utilizing the Customizer, or simply use the site title as your logo.

### Excerpt controls

Easily change the number of words in your excerpts and whether you want to show excerpts or full posts on your blog.

### Reliable Customer Support

We&#8217;re always happy to help our users and you can find loads of tutorials in the [Author Support Center](https://www.competethemes.com/documentation/author-support-center/).
---
id: 29
title: A Totally Responsive Theme
date: 2018-10-31T13:40:25+00:00
author: jjtroberts
excerpt: Don’t forget to load the site on your phone, tablet, and any other device you have. Author is a truly responsive WordPress theme.
layout: post
guid: http://www.competethemes.com/tracks-two-column-demo/?p=29
permalink: /2018/10/31/responsive-theme/
image: /wp-content/uploads/2015/02/scooter.jpeg
categories:
  - Travel
tags:
  - fashion
  - food
  - images
---
<span style="color: #000000">Proin hendrerit vestibulum blandit. Donec suscipit imperdiet arcu a vestibulum. Sed nec viverra nisl. Nulla id dapibus nulla. Curabitur tempus a libero nec condimentum. Etiam non auctor nunc. </span>

## Maecenas convallis leo tempor

Est egestas, non facilisis nisl mattis. Nunc a fermentum tortor. Nulla vehicula nunc tortor, [eu sagittis dui](http://www.competethemes.com/tracks-two-column-demo/) dictum a. Integer erat urna, gravida eu cursus non, posuere nec quam.

> Nullam sit amet orci turpis. Mauris egestas dictum porttitor.

Cras congue mi sit amet ullamcorper dapibus. Vestibulum pulvinar rutrum odio, et mollis elit blandit sit amet.

Suspendisse semper

  * elit eu volutpat tincidunt
  * ante augue sodales justo
  * auctor mattis mi dui at elit
  * duis vel porttitor tellus

Aenean vestibulum volutpat est ut vestibulum.

<img class="alignright size-medium wp-image-114" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2015/02/forest-300x200.jpeg" alt="forest" width="300" height="200" /> 

Pellentesque mauris ante, elementum sed arcu id, tincidunt venenatis neque. Aliquam consectetur magna ut adipiscing molestie. Phasellus faucibus pretium odio.

Ut ut lacus est. Nam consectetur nulla eget nibh auctor fermentum. Aenean [feugiat semper arcu](http://www.competethemes.com/tracks-two-column-demo/), id suscipit enim fermentum et. Curabitur tellus neque, eleifend et turpis dictum, lacinia porta nibh.

Nulla sed tincidunt felis. Phasellus malesuada sed metus nec tristique. Duis semper, eros nec aliquet lacinia, tortor erat interdum dui, non vestibulum massa odio et dui. Pellentesque in nibh libero. Sed at velit fermentum, porttitor nisi eu, condimentum orci.

### Aliquam vestibulum orci

sit amet risus cursus, nec lobortis magna aliquam. Fusce **in mauris neque**. Etiam vitae felis mauris. Vestibulum eu odio at velit varius hendrerit tempus vel libero. Vivamus ultricies sapien sed posuere scelerisque. Aliquam mattis eros et semper laoreet.

> Donec eget consequat sem. Suspendisse sit amet varius justo.

Cras congue ac orci quis consequat. Sed purus libero, volutpat non facilisis et, adipiscing sed lacus. Vestibulum aliquam porta quam, vel varius tortor eleifend eu.

### Donec at erat et sem bibendum hendrerit

**Vestibulum turpis augue, rhoncus a laoreet sed**, vestibulum a nunc. Fusce semper pretium massa sit amet mollis. Proin sit amet condimentum nulla, vitae cursus est. Integer id tempor elit, sed commodo est. Sed [sollicitudin nibh faucibus](http://www.competethemes.com/tracks-two-column-demo/), vulputate turpis at, imperdiet mi.

Ut venenatis mauris vel consectetur convallis. Donec scelerisque at eros eu convallis. Nulla consequat ut eros et auctor.

_Sed venenatis nisi vitae turpis dictum sollicitudin. Nunc quis metus porttitor neque luctus ultricies._
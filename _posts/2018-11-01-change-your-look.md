---
id: 41
title: A Post Without an Image
date: 2018-11-01T13:40:25+00:00
author: jjtroberts
excerpt: Don’t like using images? No problem! Every post will still look great.
layout: post
guid: http://www.competethemes.com/tracks-two-column-demo/?p=41
permalink: /2018/11/01/change-your-look/
categories:
  - Design
  - Fashion
tags:
  - fashion
  - foreign
  - wordpress
---
<span style="color: #000000">Suspendisse venenatis quam lacus, ac aliquam mauris vehicula ac. Duis pellentesque est sapien, nec porttitor enim dapibus id. Sed a adipiscing nisl, eu dictum risus. Curabitur quis egestas odio, ac tincidunt neque.</span>

<div id='gallery-3' class='gallery galleryid-41 gallery-columns-3 gallery-size-thumbnail'>
  <figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/horses-2/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2015/02/horses-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/city-blurred-2/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2015/02/city-blurred-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/moose-2/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2015/02/moose-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/above-clouds-2/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2015/02/above-clouds-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/chillin-2/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2015/02/chillin-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/jellyfish-2/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2015/02/jellyfish-150x150.jpeg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure>
</div>

This gallery was made using WordPress&#8217; built-in gallery functionality.
---
id: 47
title: Post with a Gallery
date: 2018-10-30T13:40:25+00:00
author: jjtroberts
excerpt: You can add beautiful, responsive WordPress galleries to any page or post. No plugin required!
layout: post
guid: http://www.competethemes.com/tracks-two-column-demo/?p=47
permalink: /2018/10/30/gallery-post/
image: /wp-content/uploads/2015/02/purple-volcano.jpeg
categories:
  - Design
  - News
tags:
  - food
  - random
  - wordpress
---
<span style="color: #000000">Quisque dapibus, velit eget ullamcorper suscipit, sapien ligula hendrerit lectus, vitae tristique sapien velit ac lectus. Mauris ullamcorper nisi sit amet est vestibulum interdum. Integer venenatis rutrum ipsum. Pellentesque sollicitudin turpis eu nibh ornare, eu pretium risus pharetra. </span>

<div id='gallery-2' class='gallery galleryid-47 gallery-columns-4 gallery-size-thumbnail'>
  <figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/2018/11/02/awesome-post/berries/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/berries-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/birds/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/birds-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/sample-page-2/tools/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/tools-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/ocean/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/ocean-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/2018/11/03/standard-post/clothes/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/clothes-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="" /></a>
  </div></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='http://josephjtroberts.com.ddev.local/2018/10/30/gallery-post/rasberries/'><img width="150" height="150" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/rasberries-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="raspberries in a cup" /></a>
  </div></figure>
</div>

Est egestas, non facilisis nisl mattis. Nunc a fermentum tortor. Nulla vehicula nunc tortor, [eu sagittis dui](http://www.competethemes.com/tracks-two-column-demo/) dictum a. Integer erat urna, gravida eu cursus non, posuere nec quam.

## 1. Nullam sit amet orci turpis.

Cras congue mi sit amet ullamcorper dapibus. Vestibulum pulvinar rutrum odio, et mollis elit blandit sit amet.

Suspendisse semper

  * elit eu volutpat tincidunt
  * ante augue sodales justo
  * auctor mattis mi dui at elit
  * duis vel porttitor tellus

## 2. Aenean vestibulum volutpat est ut vestibulum.

Pellentesque mauris ante, elementum sed arcu id, tincidunt venenatis neque. Aliquam consectetur magna ut adipiscing molestie.

  * Phasellus faucibus pretium odio.
  * Ut ut lacus est.
  * Nam consectetur nulla eget nibh auctor fermentum.
  * Aenean [feugiat semper arcu](http://www.competethemes.com/tracks-two-column-demo/)

id suscipit enim fermentum et. Curabitur tellus neque, eleifend et turpis dictum, lacinia porta nibh.

Nulla sed tincidunt felis.

## 3. Phasellus malesuada sed metus nec tristique

Duis semper, eros nec aliquet lacinia, tortor erat interdum dui, non vestibulum massa odio et dui. Pellentesque in nibh libero. Sed at velit fermentum, porttitor nisi eu, condimentum orci.

Sit amet risus cursus, nec lobortis magna aliquam. Fusce **in mauris neque**. Etiam vitae felis mauris. Vestibulum eu odio at velit varius hendrerit tempus vel libero. Vivamus ultricies sapien sed posuere scelerisque. Aliquam mattis eros et semper laoreet.

## Donec eget consequat sem

Cras congue ac orci quis consequat. Sed purus libero, volutpat non facilisis et, adipiscing sed lacus. Vestibulum aliquam porta quam, vel varius tortor eleifend eu.

### Donec at erat et sem bibendum hendrerit

**Vestibulum turpis augue, rhoncus a laoreet sed**, vestibulum a nunc. Integer id tempor elit, sed commodo est. Sed [sollicitudin nibh faucibus](http://www.competethemes.com/tracks-two-column-demo/), vulputate turpis at, imperdiet mi.

> Ut venenatis mauris vel consectetur convallis. Donec scelerisque at eros eu convallis. Nulla consequat ut eros et auctor. Fusce semper pretium massa sit amet mollis. Proin sit amet condimentum nulla, vitae cursus est.

_Sed venenatis nisi vitae turpis dictum sollicitudin. Nunc quis metus porttitor neque luctus ultricies._
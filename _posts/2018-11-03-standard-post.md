---
id: 44
title: This is a Standard Post
date: 2018-11-03T13:40:25+00:00
author: jjtroberts
excerpt: This is what an ordinary post looks like, and you’re reading a custom excerpt right now. Have fun looking around the theme, and don’t forget to check it out on your phone!
layout: post
guid: http://www.competethemes.com/tracks-two-column-demo/?p=44
permalink: /2018/11/03/standard-post/
image: /wp-content/uploads/2015/02/city-blurred.jpeg
categories:
  - Cooking
  - News
tags:
  - blogging
  - funny
  - new
---
<span style="color: #000000">Nam auctor a purus vel venenatis. Quisque magna nibh, cursus ac nisl nec, porta porttitor arcu. Nam sapien magna, semper ut lectus nec, fermentum rutrum arcu. Morbi quis est vitae erat aliquam adipiscing lacinia non libero. Quisque ac eros turpis.</span>

## Maecenas convallis leo tempor

Est egestas, non facilisis nisl mattis. Nunc a fermentum tortor. Nulla vehicula nunc tortor, [eu sagittis dui](http://www.competethemes.com/tracks-two-column-demo/) dictum a. Integer erat urna, gravida eu cursus non, posuere nec quam.

> Nullam sit amet orci turpis. Mauris egestas dictum porttitor.

Cras congue mi sit amet ullamcorper dapibus. Vestibulum pulvinar rutrum odio, et mollis elit blandit sit amet.

Suspendisse semper

  * elit eu volutpat tincidunt
  * ante augue sodales justo
  * auctor mattis mi dui at elit
  * duis vel porttitor tellus

Aenean vestibulum volutpat est ut vestibulum.

<img class="alignleft size-medium wp-image-71" src="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/rasberries-300x169.jpg" alt="raspberries in a cup" width="300" height="169" srcset="http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/rasberries-300x169.jpg 300w, http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/rasberries-768x432.jpg 768w, http://josephjtroberts.com.ddev.local/wp-content/uploads/2014/07/rasberries-1024x576.jpg 1024w" sizes="(max-width: 300px) 100vw, 300px" /> 

Pellentesque mauris ante, elementum sed arcu id, tincidunt venenatis neque. Aliquam consectetur magna ut adipiscing molestie. Phasellus faucibus pretium odio.

Ut ut lacus est. Nam consectetur nulla eget nibh auctor fermentum. Aenean [feugiat semper arcu](http://www.competethemes.com/tracks-two-column-demo/), id suscipit enim fermentum et. Curabitur tellus neque, eleifend et turpis dictum, lacinia porta nibh.

Nulla sed tincidunt felis. Phasellus malesuada sed metus nec tristique. Duis semper, eros nec aliquet lacinia, tortor erat interdum dui, non vestibulum massa odio et dui. Pellentesque in nibh libero. Sed at velit fermentum, porttitor nisi eu, condimentum orci.

### Aliquam vestibulum orci

sit amet risus cursus, nec lobortis magna aliquam. Fusce **in mauris neque**. Etiam vitae felis mauris. Vestibulum eu odio at velit varius hendrerit tempus vel libero. Vivamus ultricies sapien sed posuere scelerisque. Aliquam mattis eros et semper laoreet.

> Donec eget consequat sem. Suspendisse sit amet varius justo.

Cras congue ac orci quis consequat. Sed purus libero, volutpat non facilisis et, adipiscing sed lacus. Vestibulum aliquam porta quam, vel varius tortor eleifend eu.

### Donec at erat et sem bibendum hendrerit

**Vestibulum turpis augue, rhoncus a laoreet sed**, vestibulum a nunc. Fusce semper pretium massa sit amet mollis. Proin sit amet condimentum nulla, vitae cursus est. Integer id tempor elit, sed commodo est. Sed [sollicitudin nibh faucibus](http://www.competethemes.com/tracks-two-column-demo/), vulputate turpis at, imperdiet mi.

Ut venenatis mauris vel consectetur convallis. Donec scelerisque at eros eu convallis. Nulla consequat ut eros et auctor.

_Sed venenatis nisi vitae turpis dictum sollicitudin. Nunc quis metus porttitor neque luctus ultricies._